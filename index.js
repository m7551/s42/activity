console.log('Hello World')

const firstName = document.querySelector("#txt-first-name")
const lastName = document.querySelector("#txt-last-name")
const fullName = document.querySelector("#span-full-name")

function printName(event){
    fullName.innerHTML = `${firstName.value} ${lastName.value}`
}

firstName.addEventListener('keyup', printName)
lastName.addEventListener('keyup', printName)